# Client

### Installation


`yarn install`


### Run the sample


`npm run start`


### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).


# Server

### Installation


`yarn install`


#### Docker

There is a `docker-compose.yml` file for starting Docker.

`docker-compose up`

After running the sample, you can stop the Docker container with

`docker-compose down`

MongoDB database needs to be populated with some data, for that, *mongodb_bulk* bulk script has been provided.

`docker exect -it server_mongodb_1 bash`

`mongo`

`db use test`

Copy and paste bulk script and press enter. Data should be populated.

### Run the sample

Then, run Nest as usual:

`npm run start:dev`

