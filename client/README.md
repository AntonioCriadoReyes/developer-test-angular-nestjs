# Client

### Installation


`yarn install`


### Run the sample


`npm run start`


### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

