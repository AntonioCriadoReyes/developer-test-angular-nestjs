import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { ExchangeRate, RatesService } from '../services/exchangerate.service';

@Injectable({
  providedIn: 'root'
})
export class RatesResolver implements Resolve<ExchangeRate> {

    constructor(private rate: RatesService) { }

    resolve(): Observable<ExchangeRate> {
        return this.rate.find();
    }
}
