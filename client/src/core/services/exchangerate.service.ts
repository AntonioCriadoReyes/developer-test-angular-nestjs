import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Socket } from 'ngx-socket-io';

export interface ExchangeRate {
  rate: number;
  source: string;
  target: string;
  effectiveFrom: Date;
}

@Injectable({
  providedIn: 'root',
})
export class RatesService {

  private ratessUrl = 'api/rates';
  constructor(
    private socket: Socket,
    private http: HttpClient) {}

  find() {
    return this.http.get<ExchangeRate>(this.ratessUrl)
      .pipe(
        catchError(this.handleError<ExchangeRate>('Rates.find'))
      );
  }

  listen() {
    return this.socket.fromEvent('rate');
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
