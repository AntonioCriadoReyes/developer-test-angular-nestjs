import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RatesResolver } from '../../core/resolvers/exchangerate.resolver';
import { AccountsOutletComponent } from './accounts-outlet.component';
import { AccountsComponent } from './accounts.component';
import { AccountsResolver } from './accounts.resolver';


const routes: Routes = [
  {
    path: '',
    component: AccountsOutletComponent,
    resolve: {
      rate: RatesResolver
    },
    children: [
      {
        path: '',
        component: AccountsComponent,
        resolve: {
          accounts: AccountsResolver
        }
      },
      {
        path: ':id',
        loadChildren: () => import('./detail/accounts-detail.module').then(m => m.AccountsDetailModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
