import { TestBed, async } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ExchangeRate, RatesService } from '../../core/services/exchangerate.service';
import { AccountsComponent } from './accounts.component';
import { Account, AccountExtended, AccountsService } from './accounts.service';


const mockExchange: ExchangeRate = {  rate: 2, source: 'BTC', target: 'USD', effectiveFrom: new Date() };
const mockAccounts: Account[] = [
  { _id: '1', name: 'test1', category: 'category', tags: [], balance: 10, availableBalance: 10, transactions: [] },
  { _id: '2', name: 'test2', category: 'category', tags: [], balance: 0, availableBalance: 10, transactions: [] }
];
const mockAccountsExt: AccountExtended[] = [
  // tslint:disable-next-line: max-line-length
  { _id: '1', name: 'test1', category: 'category', tags: [], balance: 10, balanceUSD: 10, availableBalance: 10, availableBalanceUSD: 10, transactions: [] },
  // tslint:disable-next-line: max-line-length
  { _id: '2', name: 'test2', category: 'category', tags: [], balance: 0,  balanceUSD: 0, availableBalance: 10, availableBalanceUSD: 10, transactions: [] }
];

const route = {
  mockSnapshot: jasmine.createSpy('snapshot')
    .and.returnValue({ data: { rate: mockExchange } }),
  mockData: jasmine.createSpy('data')
    .and.returnValue(of({ accounts: mockAccounts }))
};

class ActivatedRouteStub {
  get snapshot() {
      return route.mockSnapshot();
  }

  get data() {
    return of(route.mockData());
  }
}

describe('AccountsComponent test', () => {
  const mockAccountService = {
    listen: jasmine.createSpy('listen')
      .and.returnValue(of({})),
    prepareBalanceConversion: jasmine.createSpy('prepareBalanceConversion')
      .and.returnValue(of(mockAccountsExt)),
    setAccountState: jasmine.createSpy('setAccountState')
      .and.returnValue(mockAccounts)
  };
  const mockRatesService = {
    listen: jasmine.createSpy('listen')
      .and.returnValue(of({}))
  };

  let component: AccountsComponent;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AccountsComponent
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: AccountsService, useValue: mockAccountService },
        { provide: RatesService, useValue: mockRatesService }
      ]
    }).compileComponents();
    const fixture = TestBed.createComponent(AccountsComponent);
    component = fixture.componentInstance;
  }));

  it('should create instance', () => {
    expect(component).toBeTruthy();
  });

  it('given component instance should setup initial configuration', () => {
    component.ngOnInit();

    expect(component.dataSource.data).toEqual(mockAccountsExt);
    expect(component.exchange).toEqual(mockExchange);

    expect(mockAccountService.listen).toHaveBeenCalled();
    expect(mockRatesService.listen).toHaveBeenCalled();

    expect(mockAccountService.prepareBalanceConversion).toHaveBeenCalled();
    expect(mockAccountService.setAccountState).toHaveBeenCalled();
  });
});
