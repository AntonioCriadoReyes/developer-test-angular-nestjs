import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExchangeRate } from '../../../core/services/exchangerate.service';
import { Account } from '../accounts.service';

@Component({
  selector: 'app-accounts-detail',
  templateUrl: './accounts-detail.component.html',
  styleUrls: ['./accounts-detail.component.scss']
})
export class AccountsDetailComponent implements OnInit {
  displayedColumns: string[] = ['confirmationDate', 'orderId', 'orderCode', 'type', 'debit', 'credit', 'balance'];
  exchange: ExchangeRate;
  account: Account;

  constructor(
    private route: ActivatedRoute) {}

  ngOnInit() {
    this.exchange = this.route.snapshot.parent.parent.data.rate;
    this.account = this.route.snapshot.data.account;
  }

  calculateUSDAmount(amount: number) {
    return amount * this.exchange.rate;
  }
}
