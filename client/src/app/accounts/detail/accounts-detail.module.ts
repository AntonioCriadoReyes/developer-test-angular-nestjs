import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatTableModule } from '@angular/material/table';

import { AccountsDetailRoutingModule } from './accounts-detail-routing.module';
import { AccountsDetailComponent } from './accounts-detail.component';
import { AccountsDetailResolver } from './accounts-detail.resolver';


@NgModule({
  declarations: [
    AccountsDetailComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    AccountsDetailRoutingModule
  ],
  providers: [ AccountsDetailResolver ]
})
export class AccountsDetailModule { }
