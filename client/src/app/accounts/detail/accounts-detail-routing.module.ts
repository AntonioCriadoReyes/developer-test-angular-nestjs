import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountsDetailComponent } from './accounts-detail.component';
import { AccountsDetailResolver } from './accounts-detail.resolver';


const routes: Routes = [
  {
    path: '',
    component: AccountsDetailComponent,
    resolve: {
      account: AccountsDetailResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsDetailRoutingModule { }
