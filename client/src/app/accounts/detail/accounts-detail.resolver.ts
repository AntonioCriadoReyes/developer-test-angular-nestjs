import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Account, AccountsService } from '../accounts.service';


@Injectable()
export class AccountsDetailResolver implements Resolve<Account> {

    constructor(private accounts: AccountsService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Account> {
        return this.accounts.findById(route.paramMap.get('id'));
    }
}
