import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { pluck, switchMap } from 'rxjs/operators';

import { ExchangeRate, RatesService } from '../../core/services/exchangerate.service';
import { Account, AccountsService, AccountExtended } from './accounts.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss'],
  animations: [
    trigger('highlight', [
      transition('* => POSITIVE', [
        animate('1s ease-in-out', style({ backgroundColor: 'rgba(42, 187, 155, 0.5)'})),
        animate('1s ease-in-out', style({ backgroundColor: 'inherit'}))
      ]),
      transition('* => NEGATIVE', [
        animate('1s ease-in-out', style({ backgroundColor: 'rgba(240, 52, 52, 0.5)'})),
        animate('1s ease-in-out', style({ backgroundColor: 'inherit'}))
      ]),
      transition('NEGATIVE => POSITIVE', [
        animate('1s ease-in-out', style({ backgroundColor: 'rgba(42, 187, 155, 0.5)'})),
        animate('1s ease-in-out', style({ backgroundColor: 'inherit'}))
      ]),
      transition('POSITIVE => NEGATIVE', [
        animate('1s ease-in-out', style({ backgroundColor: 'rgba(240, 52, 52, 0.5)'})),
        animate('1s ease-in-out', style({ backgroundColor: 'inherit'}))
      ]),
    ]),
  ],
})
export class AccountsComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['name', 'category', 'tags', 'balance', 'availableBalance'];
  exchange: ExchangeRate;
  dataSource: MatTableDataSource<AccountExtended>;

  @ViewChild(MatSort) private sort: MatSort;
  constructor(
    private rates: RatesService,
    private accounts: AccountsService,
    private router: Router,
    private route: ActivatedRoute) {}

  ngOnInit() {
    this.exchange = this.route.snapshot.data.rate;
    this.route.data
      .pipe(
        pluck('accounts'),
        switchMap((accs: Account[]) => this.accounts.prepareBalanceConversion(this.exchange, accs))
      ).subscribe((accs: AccountExtended[]) => this.dataSource = new MatTableDataSource(accs));

    this.rates.listen()
      .pipe(
        switchMap((exchange: ExchangeRate) => this.accounts.prepareBalanceConversion(exchange, this.dataSource.data))
      ).subscribe((accs: AccountExtended[]) => this.dataSource.data = accs);

    this.accounts.listen()
      .pipe(
        switchMap((acc: Account) => this.accounts.setAccountState(this.dataSource.data[this.findDisplayedAccount(acc)], acc)),
        switchMap((acc: Account) => this.accounts.prepareBalanceConversion(this.exchange, acc))
      ).subscribe((acc: AccountExtended) => {
        this.dataSource.data[this.findDisplayedAccount(acc)] = acc;
        this.dataSource.data = this.dataSource.data;
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  onAccountClick({ _id }: AccountExtended) {
    this.router.navigate([ _id ], { relativeTo: this.route });
  }

  trackById(index: number, item: AccountExtended) {
    return item._id;
  }

  private findDisplayedAccount(acc: Account) {
    return this.dataSource.data.findIndex((i: AccountExtended) => i._id === acc._id );
  }
}
