import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SocketIoModule } from 'ngx-socket-io';

import { Account, AccountsService } from './accounts.service';


describe('Accounts service', () => {
  let service: AccountsService;
  let httpMock: HttpTestingController;

  const mockAccounts: Account[] = [
    { _id: '1', name: 'test1', category: 'category', tags: [], balance: 10, availableBalance: 10, transactions: [] },
    { _id: '2', name: 'test2', category: 'category', tags: [], balance: 0, availableBalance: 10, transactions: [] }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule,
          SocketIoModule.forRoot({url: '', options: {}})
        ],
        providers: [
            AccountsService
        ]
    });

    service = TestBed.inject(AccountsService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('instance should be created', () => {
    expect(service).toBeTruthy();
  });

  it('when findAll accounts', () => {
    service.findAll().subscribe((accounts) => {
      expect(accounts.length).toBe(2);
      expect(accounts).toEqual(mockAccounts);
    });
    const req = httpMock.expectOne('api/accounts');
    expect(req.request.method).toBe('GET');
    req.flush(mockAccounts);
  });
});
