import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ExchangeRate, RatesService } from '../../core/services/exchangerate.service';

@Component({
  selector: 'app-accounts-outlet',
  template: `
    <mat-toolbar color="primary">
      <mat-icon aria-hidden="false" aria-label="Rate icon">monetization_on</mat-icon>&nbsp;{{ exchange.rate | currency }}
    </mat-toolbar>
    <router-outlet></router-outlet>
  `,
  styles: [`
    .mat-toolbar { justify-content: center; }
  `]
})
export class AccountsOutletComponent implements OnInit {
  exchange: ExchangeRate;

  constructor(
    private rates: RatesService,
    private route: ActivatedRoute) {}

  ngOnInit() {
    this.exchange = this.route.snapshot.data.rate;
    this.rates.listen().subscribe((data: ExchangeRate) => this.exchange = data);
  }
}
