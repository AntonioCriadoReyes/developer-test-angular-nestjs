import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Socket } from 'ngx-socket-io';

import { ExchangeRate } from '../../core/services/exchangerate.service';

export interface Transaction {
  confirmationDate: Date;
  orderId: string;
  orderCode: string;
  type: string;
  debit: number;
  credit: number;
  balance: number;
}
export interface Account {
    _id: string;
    name: string;
    category: string;
    tags: string[];
    balance: number;
    availableBalance: number;
    transactions: Transaction[];
}

export interface AccountExtended extends Account {
  balanceUSD: number;
  availableBalanceUSD: number;
}

@Injectable()
export class AccountsService {

  private accountsUrl = 'api/accounts';
  constructor(
    private socket: Socket,
    private http: HttpClient) {}

  findAll() {
    return this.http.get<Account[]>(this.accountsUrl)
      .pipe(
        catchError(this.handleError<Account[]>('Accounts.findAll', []))
      );
  }

  findById(id: string) {
    return this.http.get<Account>(`${ this.accountsUrl }/${ id }`)
      .pipe(
        catchError(this.handleError<Account>('Accounts.findById'))
      );
  }

  listen() {
    return this.socket.fromEvent('account');
  }

  prepareBalanceConversion(rate: ExchangeRate, accounts: Account[] | Account): Observable<AccountExtended[]> | Observable<AccountExtended> {
    return accounts instanceof Array ? of<AccountExtended[]>(
      accounts.map((acc: Account) =>
        // tslint:disable-next-line: no-angle-bracket-type-assertion
        <AccountExtended> {...acc, ...{ balanceUSD : acc.balance * rate.rate,
          availableBalanceUSD : acc.availableBalance * rate.rate }})
    ) : of(
      // tslint:disable-next-line: no-angle-bracket-type-assertion
      <AccountExtended> {...accounts, ...{ balanceUSD : accounts.balance * rate.rate,
          availableBalanceUSD : accounts.availableBalance * rate.rate }});
  }

  setAccountState(previous: Account, current: Account) {
    return of(
      {
        ...current,
        state: previous.availableBalance > current.availableBalance ?
          'NEGATIVE' : previous.availableBalance < current.availableBalance ? 'POSITIVE' : ''
      }
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
