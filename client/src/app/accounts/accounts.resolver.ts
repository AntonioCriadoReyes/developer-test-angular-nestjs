import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Account, AccountsService } from './accounts.service';

@Injectable()
export class AccountsResolver implements Resolve<Account[]> {

    constructor(private accounts: AccountsService) { }

    resolve(): Observable<Account[]> {
        return this.accounts.findAll();
    }
}
