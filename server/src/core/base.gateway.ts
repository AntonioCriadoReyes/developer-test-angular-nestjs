import { WebSocketServer, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';

export abstract class BaseGateway implements OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer()
    protected server;
    private connections = 0;
    private interval: NodeJS.Timeout;

    constructor(private timeout: number) {}

    async handleConnection(){
        this.connections++;
        console.log(`${ this.connections } connections established`);

        if(!this.interval) {
            this.interval = setInterval(async () => {
                await this.onHandleConnection();
            }, this.timeout);
        }
    }

    async handleDisconnect(){
        this.connections--;
        console.log(`${ this.connections } connections established`);

        if(this.interval && this.connections === 0) {
            clearInterval(this.interval);
            this.interval = null;
        }
    }

    async abstract onHandleConnection();

}