import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AccountsModule } from './accounts/accounts.module';
import { RatesModule } from './rates/rates.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/test', ),
    AccountsModule,
    RatesModule
  ],
})
export class AppModule {}
