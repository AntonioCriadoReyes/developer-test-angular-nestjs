import { Injectable } from '@nestjs/common';
import { Utils } from '../../core/utils';
import { ExchangeRate } from './domain/exchangeRate';

@Injectable()
export class RatesService {

  async find(): Promise<ExchangeRate> {
    return new Promise((resolve) => {
        resolve({ rate: Utils.getRndInteger(2000, 5000), target: "USD", source: "BTC", effectiveFrom: new Date() })
    });
  }
}
