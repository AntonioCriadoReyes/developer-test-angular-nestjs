export interface ExchangeRate { 
    rate: number;

    source: string;

    target: string;

    effectiveFrom: Date;
}
