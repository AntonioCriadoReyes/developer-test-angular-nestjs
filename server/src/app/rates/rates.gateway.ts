import { WebSocketGateway } from '@nestjs/websockets';

import { BaseGateway } from '../../core/base.gateway';
import { RatesService } from './rates.service';

@WebSocketGateway()
export class RatesGateway extends BaseGateway {

    constructor(private rates: RatesService) {
        super(30000);
    }

    async onHandleConnection() {
        this.server.emit('rate', await this.rates.find());
    }
}