import { Controller, Get } from '@nestjs/common';

import { RatesService } from './rates.service';
import { ExchangeRate } from "./domain/exchangeRate";

@Controller('api/rates')
export class RatesController {
  constructor(private readonly ratesService: RatesService) {}

  @Get()
  async find(): Promise<ExchangeRate> {
    return this.ratesService.find();
  }
}