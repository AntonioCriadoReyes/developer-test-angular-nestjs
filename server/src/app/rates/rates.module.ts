import { Module } from '@nestjs/common';

import { RatesController } from './rates.controller';
import { RatesGateway } from './rates.gateway';
import { RatesService } from './rates.service';

@Module({
  imports: [ ],
  controllers: [ RatesController ],
  providers: [ RatesService, RatesGateway ],
})
export class RatesModule {}