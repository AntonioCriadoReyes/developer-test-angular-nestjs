import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

import { Transaction, TransactionSchema } from "./transaction.schema";

@Schema()
export class Account {

    @Prop()
    name: string;

    @Prop()
    category: string;

    @Prop([String])
    tags: string[];

    @Prop()
    balance: number;

    @Prop()
    availableBalance: number;

    @Prop([TransactionSchema])
    transactions: Transaction[];
}

export type AccountDocument = Account & Document;

export const AccountSchema = SchemaFactory.createForClass(Account);
