import { Prop, SchemaFactory } from "@nestjs/mongoose";

export class Transaction {

    @Prop()
    confirmationDate: Date;

    @Prop()
    orderId: string;

    @Prop()
    orderCode: string;

    @Prop()
    type: string;

    @Prop()
    debit: number;

    @Prop()
    credit: number;

    @Prop()
    balance: number;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);

