import { WebSocketGateway } from '@nestjs/websockets';
import { BaseGateway } from '../../core/base.gateway';
import { AccountsService } from './accounts.service';


@WebSocketGateway()
export class AccountsGateway extends BaseGateway {

    constructor(private accounts: AccountsService) {
        super(40000);
    }

    async onHandleConnection() {
        this.server.emit('account', await this.accounts.updatedBalanceRandomAccount());
    }
}

