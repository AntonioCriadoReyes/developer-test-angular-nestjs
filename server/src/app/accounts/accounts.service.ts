import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { Account, AccountDocument } from './schemas/account.schema';
import { Utils } from '../../core/utils';

@Injectable()
export class AccountsService {
  constructor(@InjectModel('Account') private accountModel: Model<AccountDocument>) {}

  async findAll(): Promise<Account[]> {
    return this.accountModel.find().exec();
  }

  async findById(id: string): Promise<Account> {
    return this.accountModel.findById(id).exec();
  }

  async updatedBalanceRandomAccount() {
    const accs = await this.findAll();
    const account = accs[Utils.getRndInteger(0, accs.length - 1)];
    account.availableBalance = Utils.getRndInteger(0, 400);

    return account;
  }
}
